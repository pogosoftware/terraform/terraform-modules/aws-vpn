variable "tags" {
  type        = map(string)
  description = "Set of tags."
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "(Dependency) The VPC ID."
}

variable "vpc_public_rt_ids" {
  type        = set(string)
  description = "The public vpc route table ids."
}

variable "vpc_private_rt_ids" {
  type        = set(string)
  description = "The private vpc route table ids."
}

# CGW
variable "cgw_bgp_asn" {
  type        = number
  description = "The gateway's Border Gateway Protocol (BGP) Autonomous System Number (ASN)."
  default     = 65000
}

variable "cgw_ip_address" {
  type        = string
  description = "The IP address of the gateway's Internet-routable external interface."
}

variable "cgw_type" {
  type        = string
  description = "The type of customer gateway. The only type AWS supports at this time is \"ipsec.1\"."
  default     = "ipsec.1"
}

# VPN Connection
variable "vpn_connection_static_routes_only" {
  type        = bool
  description = "Whether the VPN connection uses static routes exclusively. Static routes must be used for devices that don't support BGP."
  default     = true
}

variable "vpn_connection_route_destination_cidr_block" {
  type        = string
  description = "The CIDR block associated with the local subnet of the customer network."
}

variable "vpn_connection_tunnel1_preshared_key" {
  description = "The preshared key of the first VPN tunnel."
  type        = string
  default     = ""
}

variable "vpn_connection_tunnel2_preshared_key" {
  description = "The preshared key of the second VPN tunnel."
  type        = string
  default     = ""
}