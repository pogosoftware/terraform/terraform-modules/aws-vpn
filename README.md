# Terraform module to manage aws vpn

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.12.6 |
| aws | >= 2.41 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.41 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| cgw\_bgp\_asn | The gateway's Border Gateway Protocol (BGP) Autonomous System Number (ASN). | `number` | `65000` | no |
| cgw\_ip\_address | The IP address of the gateway's Internet-routable external interface. | `string` | n/a | yes |
| cgw\_type | The type of customer gateway. The only type AWS supports at this time is "ipsec.1". | `string` | `"ipsec.1"` | no |
| tags | Set of tags. | `map(string)` | `{}` | no |
| vpc\_id | (Dependency) The VPC ID. | `string` | n/a | yes |
| vpc\_private\_rt\_ids | The private vpc route table ids. | `set(string)` | n/a | yes |
| vpc\_public\_rt\_ids | The public vpc route table ids. | `set(string)` | n/a | yes |
| vpn\_connection\_route\_destination\_cidr\_block | The CIDR block associated with the local subnet of the customer network. | `string` | n/a | yes |
| vpn\_connection\_static\_routes\_only | Whether the VPN connection uses static routes exclusively. Static routes must be used for devices that don't support BGP. | `bool` | `true` | no |
| vpn\_connection\_tunnel1\_preshared\_key | The preshared key of the first VPN tunnel. | `string` | `""` | no |
| vpn\_connection\_tunnel2\_preshared\_key | The preshared key of the second VPN tunnel. | `string` | `""` | no |

## Outputs

No output.

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->