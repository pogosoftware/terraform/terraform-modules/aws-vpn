resource "aws_vpn_gateway" "this" {
  vpc_id = var.vpc_id
  tags   = var.tags
}

resource "aws_vpn_gateway_route_propagation" "public" {
  for_each       = var.vpc_public_rt_ids
  vpn_gateway_id = aws_vpn_gateway.this.id
  route_table_id = each.key
}

resource "aws_vpn_gateway_route_propagation" "private" {
  for_each       = var.vpc_private_rt_ids
  vpn_gateway_id = aws_vpn_gateway.this.id
  route_table_id = each.key
}

resource "aws_customer_gateway" "this" {
  bgp_asn    = var.cgw_bgp_asn
  ip_address = var.cgw_ip_address
  type       = var.cgw_type
  tags       = var.tags
}

resource "aws_vpn_connection" "this" {
  vpn_gateway_id      = aws_vpn_gateway.this.id
  customer_gateway_id = aws_customer_gateway.this.id

  type               = var.cgw_type
  static_routes_only = var.vpn_connection_static_routes_only

  tunnel1_preshared_key = var.vpn_connection_tunnel1_preshared_key
  tunnel2_preshared_key = var.vpn_connection_tunnel2_preshared_key

  tags = var.tags
}

resource "aws_vpn_connection_route" "office" {
  destination_cidr_block = var.vpn_connection_route_destination_cidr_block
  vpn_connection_id      = aws_vpn_connection.this.id
}
